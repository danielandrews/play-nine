# Play Nine

A game created using React.js. You can play the live version [here](http://sordid-map.surge.sh/)!

## Game Play

The goal of this game is to use each number only once during the game and not run out of number options.

Each turn, you get a random number of stars. Select the number or numbers that add up to that number of stars, then click the equal sign. If you chose correctly, the button will turn green, and you can click it again to submit. You can deselect numbers by clicking on them (in the top right box).

If you have no options left for achieving the sum of the number of stars, you can click the orange redraw button for a new random number of stars. Be careful though! You only get 5 redraws per game. **Good luck!**

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

import React, { Component } from 'react';
import './App.css';
var _ = require('lodash');

var possibleCombinationSum = function(arr, n) {
  if (arr.indexOf(n) >= 0) { return true; }
  if (arr[0] > n) { return false; }
  if (arr[arr.length - 1] > n) {
    arr.pop();
    return possibleCombinationSum(arr, n);
  }
  var listSize = arr.length, combinationsCount = (1 << listSize)
  for (var i = 1; i < combinationsCount ; i++ ) {
    var combinationSum = 0;
    for (var j=0 ; j < listSize ; j++) {
      if (i & (1 << j)) { combinationSum += arr[j]; }
    }
    if (n === combinationSum) { return true; }
  }
  return false;
};

const Answer = (props) => {
  return (
    <div id="answer-frame">
      <div className="well">
        {props.selectedNumbers.map((number, i) =>
          <span key={i} onClick={() => props.unselectNumber(number)}>{number}</span>
        )}  
      </div>
    </div>
  );
};

const Button = (props) => {

  let button;

  switch(props.answerIsCorrect){
    case true:
      button =
        <button type="button" className="btn btn-success btn-lg" onClick={props.acceptAnswer}>
          <i className="fa fa-check"></i>
        </button>  
      break;
    
    case false:
      button =
        <button type="button" className="btn btn-danger btn-lg">
          <i className="fa fa-times"></i>
        </button>  
      break;

    default:
      button =
        <button type="button" 
                className="btn-primary btn-lg" 
                onClick={props.checkAnswer}
                disabled={props.selectedNumbers.length === 0}>
          =
        </button>  
      break;  
  }

  return (
    <div id="button-frame" className="col-2 text-center">
      {button}
      <br />
      <br />
      <button type="button"
              className="btn btn-warning btn-sm" 
              onClick={props.redraw}
              disabled={props.redrawsAllowed === 0}>
        <i className="fa fa-refresh"></i> {props.redrawsAllowed}
      </button>
    </div>
  );
};

const Numbers = (props) => {

  const numberClassName = (number) => {
    if (props.usedNumbers.indexOf(number) >= 0){
      return 'used';
    }
    if (props.selectedNumbers.indexOf(number) >= 0){
      return 'selected';
    }
    else {
      return 'number';
    }
  }
  
  return (
    <div id="numbers-frame">
      <div className="well">
        {Numbers.list.map((number, i) =>
           <span key={i} className={numberClassName(number)}
                 onClick={() => props.selectNumber(number)}>
              {number}
           </span>
        )}
      </div>  
    </div>
  );
};

Numbers.list = _.range(1,10);

const Stars = (props) => {
  return (
    <div id="stars-frame">
      <div className="well">
        {_.range(props.numberOfStars).map(i =>
          <i key={i} className="fa fa-star"></i>
        )}
      </div>
    </div>
  );
};

const DoneFrame = (props) => {
  return (
    <div className="well text-center">
      <h2>{props.doneStatus}</h2>
      <button type="button" 
              className="btn btn-success"
              onClick={props.resetGame}>Play Again</button>
    </div>
  );
}

class Game extends Component {

  static randomNumber = () => 1 + Math.floor(Math.random() * 9);

  static initialState = () => ({
    selectedNumbers: [],
    randomNumberOfStars: Game.randomNumber(),
    usedNumbers: [],
    answerIsCorrect: null,
    redrawsAllowed : 5,
    doneStatus: null
  });

  state = Game.initialState();

  selectNumber = (clickedNumber) => {
    
    if(this.state.selectedNumbers.indexOf(clickedNumber) >= 0) {
      return;
    }

    this.setState(prevState => ({
      answerIsCorrect: null,
      selectedNumbers : prevState.selectedNumbers.concat(clickedNumber)
    }))
  };

   unselectNumber = (clickedNumber) => {

    this.setState(prevState => ({
      answerIsCorrect: null,
      selectedNumbers : prevState.selectedNumbers
      .filter(number => number !== clickedNumber)
    }))
  };

  checkAnswer = () => {
    this.setState(prevState => ({
      answerIsCorrect: prevState.randomNumberOfStars ===
        prevState.selectedNumbers.reduce((acc, n) => acc + n, 0)
    }));
  };

  acceptAnswer = () => {
    this.setState(prevState => ({
      usedNumbers: prevState.usedNumbers.concat(prevState.selectedNumbers),
      selectedNumbers : [],
      answerIsCorrect: null,
      randomNumberOfStars: Game.randomNumber()
    }), this.updateDoneStatus );
  };

  redraw = () => {

    if(this.state.redrawsAllowed === 0){
      return;
    }

    this.setState(prevState => ({
      randomNumberOfStars: Game.randomNumber(),
      selectedNumbers : [],
      answerIsCorrect: null,
      redrawsAllowed: prevState.redrawsAllowed - 1
    }), this.updateDoneStatus);
  };

  possibleSolutions = ({randomNumberOfStars, usedNumbers}) => {
    const possibleNumbers = _.range(1, 10).filter(number => 
      usedNumbers.indexOf(number) === -1
    );

    return possibleCombinationSum(possibleNumbers, randomNumberOfStars);
  };

  updateDoneStatus = () => {
    this.setState(prevState => {
      if (prevState.usedNumbers.length === 9){
        return { doneStatus: 'Done, Nice!' };
      }
      if (prevState.redrawsAllowed === 0 && !this.possibleSolutions(prevState)){
        return { doneStatus: 'Game Over!' };
      }
    });
  };

  resetGame = () => this.setState(Game.initialState());
  
  render() {

    const { 
      selectedNumbers, 
      randomNumberOfStars, 
      answerIsCorrect,
      usedNumbers,
      redrawsAllowed,
      doneStatus 
    } = this.state;

    return (
      <div id="game" className="container">
        <h2>Play Nine</h2>
        <p>The goal of this game is to use each number only once during the game and not run out of number options.
        </p>
        <p>
          Each turn, you get a random number of stars. Select the number or numbers that add up to that number of stars, then click the equal sign. If you chose correctly, the button will turn green, and you can click it again to submit. You can deselect numbers by clicking on them (in the top right box).
        </p>
        <p>
          If you have no options left for achieving the sum of the number of stars, you can click the orange redraw button for a new random number of stars. Be careful though! You only get 5 redraws per game. <strong>Good luck!</strong>
        </p>
        <hr />
        <div className="row">
          <Stars numberOfStars={randomNumberOfStars}/>
          <Button selectedNumbers={selectedNumbers} 
                  checkAnswer={this.checkAnswer}
                  answerIsCorrect={answerIsCorrect}
                  redraw={this.redraw}
                  redrawsAllowed={redrawsAllowed}
                  acceptAnswer={this.acceptAnswer}/>
          <Answer selectedNumbers={selectedNumbers}
                  unselectNumber={this.unselectNumber} />
        </div>
        <br />
        {doneStatus ?
          <DoneFrame resetGame={this.resetGame} doneStatus={doneStatus} /> :
          <Numbers selectedNumbers={selectedNumbers}
                 selectNumber={this.selectNumber}
                 usedNumbers={usedNumbers} />
        }
      </div>
    );
  }
};

class App extends Component {
  render() {
    return (
      <div>
        <Game />
      </div>
    );
  }
};

export default App;
